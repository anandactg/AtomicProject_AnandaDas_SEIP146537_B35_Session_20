<?php
namespace App;

class Person
{
    public $name="Ananda";
    public $gender="Male";
    public $blood_group="O+";

    public function showPersonInfo(){

        echo $this->name."<br>";
        echo $this->gender."<br>";
        echo $this->blood_group."<br>";
    }
}