<?php
use App\Person;
use App\Student;

//require_once("../../src/BITM/SEIP146537/Person.php");     need not to write this if we use autoload magic method.
//require_once("../../src/BITM/SEIP146537/Student.php");    need not to write this if we use autoload magic method.

function __autoload($className){
    echo $className."<br>";                  // output :  App\Person


    list($ns,$cn)=explode("\\",$className);                          // we got App\Person in $className
    require_once("../../src/BITM/SEIP146537/".$cn.".php");             //  we could write this code require_once("../../src/BITM/SEIP146537/"Person.php");
        //or

    //  $arr=explode("\\",$className);                          // we got App\Person in $className
    require_once("../../src/BITM/SEIP146537/".$arr.".php");                                                                // ns= namespace , cn = class name.
}


$objPerson= new Person();
echo $objPerson->showPersonInfo();
echo"<br>";

$objStudent=new Student();
echo $objStudent->showStudentInfo();